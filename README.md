# HOW TO RUN THIS PROJECT
1. git clone https://gitlab.com/bootcamp-batch1/basic-api.git
2. cd basic-api/
3. npm install -G nodemon
4. npm install -G ts-node
5. npm install
6. npm run dev



# Available Endpoints

## User
1. INSERT: [POST]: http://localhost:3300/user
2. FETCH USER BY ID: [GET]: http://localhost:3300/user/{id}



## Post
1. INSERT: [POST]: http://localhost:3300/post
2. FETCH ALL POST: [GET]: http://localhost:3300/post
3. UPDATE POST BY ID: [PUT]: http://localhost:3300/post/{id}
4. DELETE POST BY ID: [DELETE]: http://locallhost:3300/post