import express from 'express';
const app = express();
const PORT = 3300;
import UserRouter from './src/route/UserRoute'
import PostRouter from './src/route/PostRoute';


app.use(express.json());
app.use("/user",UserRouter);
app.use("/post",PostRouter);
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});