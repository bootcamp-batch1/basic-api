import { Request, Response } from "express";

import { IPost, PostModel } from "../model/PostModel";

export const PostController = {
  deletePost: async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      const reponse = await PostModel.delete({ _id: id });
      res.status(200).send({ action: "Deleted", post: reponse });
    } catch (error: any) {
      res.status(500).send({ msg: error.message });
    }
  },
  updatePost: async (req: Request, res: Response) => {
    try {
      const { body } = req;
      const { id } = req.params;
      const post = {
        userId: body.userId,
        title: body.title,
        message: body.message,
      };

      const reponse = await PostModel.update({ _id: id }, post);
      res.status(200).send({ action: "Updated", post: reponse });
    } catch (error: any) {
      res.status(500).send({ msg: error.message });
    }
  },
  getPostById: async (req: Request, res: Response) => {
    try {
      const { id } = req.params;
      PostModel.find({ _id: id })
        .then((post) => {
          res.status(200).send(post);
        })
        .catch((error) => {
          res.status(500).send({ msg: error.message });
        });
    } catch (error: any) {
      res.status(500).send({ msg: error.message });
    }
  },
  getAllPosts: async (req: Request, res: Response) => {
    try {
      PostModel.fetch()
        .then((posts: any) => {
          res.status(200).send(posts);
        })
        .catch((error) => {
          res.status(500).send({ msg: error.message });
        });
    } catch (error: any) {
      res.status(500).send({ msg: error.message });
    }
  },
  getPosts: async (req: Request, res: Response) => {
    try {
      PostModel.fetch()
        .then((posts: any) => {
          const newPosts = posts.filter((post: IPost) => post.status === "ACTIVE");
          res.status(200).send(newPosts);
        })
        .catch((error) => {
          res.status(500).send({ msg: error.message });
        });
    } catch (error: any) {
      res.status(500).send({ msg: error.message });
    }
  },
  insertPost: async (req: Request, res: Response) => {
    try {
      const { body } = req;
      const post = {
        userId: body.userId,
        title: body.title,
        message: body.message,
        createdDate: new Date().toISOString(),
        updatedDate: new Date().toISOString(),
        status: 'ACTIVE' as 'ACTIVE' | 'INACTIVE'
      };

      const reponse = await PostModel.insert(post);
      res.status(200).send({ action: "Added", post: reponse });
    } catch (error: any) {
      res.status(500).send({ msg: error.message });
    }
  },
};
