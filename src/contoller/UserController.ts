import { Request, Response } from 'express';

import { UserModel } from '../model/UserModel';


export const userController = {
    getUserById: async(req: Request, res: Response) => {
        try {
            const { id } = req.params;
            UserModel.find({_id: id}).then((user) => {
                res.status(200).send(user);
            }).catch((error) => {
                res.status(500).send({msg: error.message});
            });
        } catch (error: any) {
            res.status(500).send({msg: error.message});
        }
    },
    getUsers: async(req: Request, res: Response) => {
        try {
            UserModel.fetch().then((users) => {
                res.status(200).send(users);
            }).catch((error) => {
                res.status(500).send({msg: error.message});
            });
        } catch (error: any) {
            res.status(500).send({msg: error.message});
        }
    },
    insertUser: async (req: Request, res: Response) => {
        try {
            const { body } = req;
            const user = {
                id: body.id,
                name: body.name,
                email: body.email,
                password: body.password,
                role: body.role
            };
    
            const reponse =  await UserModel.insert(user);
            res.status(200).send({action: 'Added', user: reponse});
        } catch (error: any) {
            res.status(500).send({msg: error.message});
        }
      
        
    }
}