const Datastore = require("nedb"),
  db = new Datastore({ filename: "src/db/PostDB.db", autoload: true });

export interface IPost {
  userId: string;
  title: string;
  message: string;
  createdDate?: string;
  updatedDate?: string;
  status: 'ACTIVE' | 'INACTIVE';
}

export const PostModel = {
  delete: (query: any) => {
    const newUpdate = {
      status: 'INACTIVE',
      updatedDate: new Date().toISOString(),
    };
    return new Promise((resolve, reject) => {
      db.update(query, {$set: newUpdate}, {}, (err: Error, numReplaced: number) => {
        if (err) {
          reject(err);
        }
        resolve(numReplaced);
      });
    });
  },
  update: (query: any, update: any) => {
    const newUpdate = { ...update, updatedDate: new Date().toISOString() };
    return new Promise((resolve, reject) => {
      db.update(query, {$set: newUpdate}, {}, (err: Error, numReplaced: number) => {
        if (err) {
          reject(err);
        }
        resolve(numReplaced);
      });
    });
  },
  insert: (post: IPost) => {
    return new Promise((resolve, reject) => {
      db.insert(post, (err: Error, newPost: IPost) => {
        if (err) {
          reject(err);
        }
        resolve(post);
      });
    });
  },
  fetch: () => {
    return new Promise((resolve, reject) => {
      db.find({}, (err: Error, posts: IPost[]) => {
        if (err) {
          reject(err);
        }
        resolve(posts);
      });
    });
  },
  find: (query: any) => {
    return new Promise((resolve, reject) => {
      db.find(query, (err: Error, posts: IPost[]) => {
        if (err) {
          reject(err);
        }
        resolve(posts);
      });
    });
  },
};
