const Datastore = require('nedb'), db = new Datastore({ filename: 'src/db/UserDB.db', autoload: true });

enum Role {
    ADMIN = 'admin',
    LEAD = 'lead',
    DEV = 'dev',
}

interface User {
    name: string;
    email: string;
    password: string;
    role: Role;
}

export const UserModel = {
    insert: (user: User) => {
        return new Promise((resolve, reject) => {
            db.insert(user, (err:Error, newUser:User) => {
                if (err) {
                    reject(err);
                }
                resolve(newUser);
            });
        });
    },
    fetch: () => {
        return new Promise((resolve, reject) => {
            db.find({}, (err:Error, users:User[]) => {
                if (err) {
                    reject(err);
                }
                resolve(users);
            });
        });
    },
    find: (query: any) => {
        return new Promise((resolve, reject) => {
            db.find(query, (err:Error, users:User[]) => {
                if (err) {
                    reject(err);
                }
                resolve(users);
            });
        });
    },
}

