import express from 'express';
const PostRouter = express.Router();

import { PostController } from '../contoller/PostController';


PostRouter.post('/', PostController.insertPost);
PostRouter.get('/allPost', PostController.getAllPosts);
PostRouter.get('/', PostController.getPosts);
PostRouter.get('/:id', PostController.getPostById);
PostRouter.put('/:id', PostController.updatePost);
PostRouter.delete('/:id', PostController.deletePost);


export default PostRouter;