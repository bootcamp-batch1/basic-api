import express from 'express';
const UserRouter = express.Router();

import { userController } from '../contoller/UserController';


UserRouter.post('/', userController.insertUser);
UserRouter.get('/', userController.getUsers);
UserRouter.get('/:id', userController.getUserById);



export default UserRouter;